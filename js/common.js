import Vue from 'vue';
import header from '../component/header.vue';
import index from '../component/index.vue';
import footer from '../component/footer.vue';

let head = new Vue({
  el: 'body',
  components: {
    'vue-header': header,
    'vue-index': index,
    'vue-footer': footer
  }
});
